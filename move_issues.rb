require 'gitlab'
require 'pry'

Gitlab.endpoint = 'https://gitlab.com/api/v4'
Gitlab.private_token = ENV['PRIVATE_TOKEN']

infra = Gitlab.project('gitlab-com/infrastructure')
db = Gitlab.project('gitlab-com/database')

Gitlab.issues(db.id).auto_paginate do |issue|
  next if issue.state == "closed"
  puts "Moving #{issue.title} (#{issue.iid})"
  Gitlab.move_issue(db.id, issue.iid, to_project_id: infra.id)
end

binding.pry
